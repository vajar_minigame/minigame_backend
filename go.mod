module gitlab.com/vajar_minigame/minigame_backend

go 1.16

require (
	entgo.io/ent v0.9.1
	github.com/dapr/go-sdk v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-colorable v0.1.8
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a // indirect
	golang.org/x/sys v0.0.0-20210902050250-f475640dd07b // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210831024726-fe130286e0e2 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
