// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/vajar_minigame/minigame_backend/ent/bodyvalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/monster"
)

// BodyValuesCreate is the builder for creating a BodyValues entity.
type BodyValuesCreate struct {
	config
	mutation *BodyValuesMutation
	hooks    []Hook
}

// SetRemainingSaturation sets the "remaining_saturation" field.
func (bvc *BodyValuesCreate) SetRemainingSaturation(f float64) *BodyValuesCreate {
	bvc.mutation.SetRemainingSaturation(f)
	return bvc
}

// SetMaxSaturation sets the "max_saturation" field.
func (bvc *BodyValuesCreate) SetMaxSaturation(f float64) *BodyValuesCreate {
	bvc.mutation.SetMaxSaturation(f)
	return bvc
}

// SetMass sets the "mass" field.
func (bvc *BodyValuesCreate) SetMass(i int) *BodyValuesCreate {
	bvc.mutation.SetMass(i)
	return bvc
}

// AddMonsterIDs adds the "monster" edge to the Monster entity by IDs.
func (bvc *BodyValuesCreate) AddMonsterIDs(ids ...int) *BodyValuesCreate {
	bvc.mutation.AddMonsterIDs(ids...)
	return bvc
}

// AddMonster adds the "monster" edges to the Monster entity.
func (bvc *BodyValuesCreate) AddMonster(m ...*Monster) *BodyValuesCreate {
	ids := make([]int, len(m))
	for i := range m {
		ids[i] = m[i].ID
	}
	return bvc.AddMonsterIDs(ids...)
}

// Mutation returns the BodyValuesMutation object of the builder.
func (bvc *BodyValuesCreate) Mutation() *BodyValuesMutation {
	return bvc.mutation
}

// Save creates the BodyValues in the database.
func (bvc *BodyValuesCreate) Save(ctx context.Context) (*BodyValues, error) {
	var (
		err  error
		node *BodyValues
	)
	if len(bvc.hooks) == 0 {
		if err = bvc.check(); err != nil {
			return nil, err
		}
		node, err = bvc.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*BodyValuesMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = bvc.check(); err != nil {
				return nil, err
			}
			bvc.mutation = mutation
			node, err = bvc.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(bvc.hooks) - 1; i >= 0; i-- {
			mut = bvc.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, bvc.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (bvc *BodyValuesCreate) SaveX(ctx context.Context) *BodyValues {
	v, err := bvc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// check runs all checks and user-defined validators on the builder.
func (bvc *BodyValuesCreate) check() error {
	if _, ok := bvc.mutation.RemainingSaturation(); !ok {
		return &ValidationError{Name: "remaining_saturation", err: errors.New("ent: missing required field \"remaining_saturation\"")}
	}
	if _, ok := bvc.mutation.MaxSaturation(); !ok {
		return &ValidationError{Name: "max_saturation", err: errors.New("ent: missing required field \"max_saturation\"")}
	}
	if _, ok := bvc.mutation.Mass(); !ok {
		return &ValidationError{Name: "mass", err: errors.New("ent: missing required field \"mass\"")}
	}
	return nil
}

func (bvc *BodyValuesCreate) sqlSave(ctx context.Context) (*BodyValues, error) {
	_node, _spec := bvc.createSpec()
	if err := sqlgraph.CreateNode(ctx, bvc.driver, _spec); err != nil {
		if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	return _node, nil
}

func (bvc *BodyValuesCreate) createSpec() (*BodyValues, *sqlgraph.CreateSpec) {
	var (
		_node = &BodyValues{config: bvc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: bodyvalues.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: bodyvalues.FieldID,
			},
		}
	)
	if value, ok := bvc.mutation.RemainingSaturation(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeFloat64,
			Value:  value,
			Column: bodyvalues.FieldRemainingSaturation,
		})
		_node.RemainingSaturation = value
	}
	if value, ok := bvc.mutation.MaxSaturation(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeFloat64,
			Value:  value,
			Column: bodyvalues.FieldMaxSaturation,
		})
		_node.MaxSaturation = value
	}
	if value, ok := bvc.mutation.Mass(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  value,
			Column: bodyvalues.FieldMass,
		})
		_node.Mass = value
	}
	if nodes := bvc.mutation.MonsterIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: true,
			Table:   bodyvalues.MonsterTable,
			Columns: []string{bodyvalues.MonsterColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: monster.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// BodyValuesCreateBulk is the builder for creating many BodyValues entities in bulk.
type BodyValuesCreateBulk struct {
	config
	builders []*BodyValuesCreate
}

// Save creates the BodyValues entities in the database.
func (bvcb *BodyValuesCreateBulk) Save(ctx context.Context) ([]*BodyValues, error) {
	specs := make([]*sqlgraph.CreateSpec, len(bvcb.builders))
	nodes := make([]*BodyValues, len(bvcb.builders))
	mutators := make([]Mutator, len(bvcb.builders))
	for i := range bvcb.builders {
		func(i int, root context.Context) {
			builder := bvcb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*BodyValuesMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, bvcb.builders[i+1].mutation)
				} else {
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, bvcb.driver, &sqlgraph.BatchCreateSpec{Nodes: specs}); err != nil {
						if cerr, ok := isSQLConstraintError(err); ok {
							err = cerr
						}
					}
				}
				mutation.done = true
				if err != nil {
					return nil, err
				}
				id := specs[i].ID.Value.(int64)
				nodes[i].ID = int(id)
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, bvcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (bvcb *BodyValuesCreateBulk) SaveX(ctx context.Context) []*BodyValues {
	v, err := bvcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}
