// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"database/sql/driver"
	"errors"
	"fmt"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/vajar_minigame/minigame_backend/ent/activityvalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/monster"
	"gitlab.com/vajar_minigame/minigame_backend/ent/predicate"
)

// ActivityValuesQuery is the builder for querying ActivityValues entities.
type ActivityValuesQuery struct {
	config
	limit      *int
	offset     *int
	unique     *bool
	order      []OrderFunc
	fields     []string
	predicates []predicate.ActivityValues
	// eager-loading edges.
	withMonster *MonsterQuery
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the ActivityValuesQuery builder.
func (avq *ActivityValuesQuery) Where(ps ...predicate.ActivityValues) *ActivityValuesQuery {
	avq.predicates = append(avq.predicates, ps...)
	return avq
}

// Limit adds a limit step to the query.
func (avq *ActivityValuesQuery) Limit(limit int) *ActivityValuesQuery {
	avq.limit = &limit
	return avq
}

// Offset adds an offset step to the query.
func (avq *ActivityValuesQuery) Offset(offset int) *ActivityValuesQuery {
	avq.offset = &offset
	return avq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (avq *ActivityValuesQuery) Unique(unique bool) *ActivityValuesQuery {
	avq.unique = &unique
	return avq
}

// Order adds an order step to the query.
func (avq *ActivityValuesQuery) Order(o ...OrderFunc) *ActivityValuesQuery {
	avq.order = append(avq.order, o...)
	return avq
}

// QueryMonster chains the current query on the "monster" edge.
func (avq *ActivityValuesQuery) QueryMonster() *MonsterQuery {
	query := &MonsterQuery{config: avq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := avq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := avq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(activityvalues.Table, activityvalues.FieldID, selector),
			sqlgraph.To(monster.Table, monster.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, true, activityvalues.MonsterTable, activityvalues.MonsterColumn),
		)
		fromU = sqlgraph.SetNeighbors(avq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first ActivityValues entity from the query.
// Returns a *NotFoundError when no ActivityValues was found.
func (avq *ActivityValuesQuery) First(ctx context.Context) (*ActivityValues, error) {
	nodes, err := avq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{activityvalues.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (avq *ActivityValuesQuery) FirstX(ctx context.Context) *ActivityValues {
	node, err := avq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first ActivityValues ID from the query.
// Returns a *NotFoundError when no ActivityValues ID was found.
func (avq *ActivityValuesQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = avq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{activityvalues.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (avq *ActivityValuesQuery) FirstIDX(ctx context.Context) int {
	id, err := avq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single ActivityValues entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when exactly one ActivityValues entity is not found.
// Returns a *NotFoundError when no ActivityValues entities are found.
func (avq *ActivityValuesQuery) Only(ctx context.Context) (*ActivityValues, error) {
	nodes, err := avq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{activityvalues.Label}
	default:
		return nil, &NotSingularError{activityvalues.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (avq *ActivityValuesQuery) OnlyX(ctx context.Context) *ActivityValues {
	node, err := avq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only ActivityValues ID in the query.
// Returns a *NotSingularError when exactly one ActivityValues ID is not found.
// Returns a *NotFoundError when no entities are found.
func (avq *ActivityValuesQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = avq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = &NotSingularError{activityvalues.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (avq *ActivityValuesQuery) OnlyIDX(ctx context.Context) int {
	id, err := avq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of ActivityValuesSlice.
func (avq *ActivityValuesQuery) All(ctx context.Context) ([]*ActivityValues, error) {
	if err := avq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return avq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (avq *ActivityValuesQuery) AllX(ctx context.Context) []*ActivityValues {
	nodes, err := avq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of ActivityValues IDs.
func (avq *ActivityValuesQuery) IDs(ctx context.Context) ([]int, error) {
	var ids []int
	if err := avq.Select(activityvalues.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (avq *ActivityValuesQuery) IDsX(ctx context.Context) []int {
	ids, err := avq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (avq *ActivityValuesQuery) Count(ctx context.Context) (int, error) {
	if err := avq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return avq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (avq *ActivityValuesQuery) CountX(ctx context.Context) int {
	count, err := avq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (avq *ActivityValuesQuery) Exist(ctx context.Context) (bool, error) {
	if err := avq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return avq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (avq *ActivityValuesQuery) ExistX(ctx context.Context) bool {
	exist, err := avq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the ActivityValuesQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (avq *ActivityValuesQuery) Clone() *ActivityValuesQuery {
	if avq == nil {
		return nil
	}
	return &ActivityValuesQuery{
		config:      avq.config,
		limit:       avq.limit,
		offset:      avq.offset,
		order:       append([]OrderFunc{}, avq.order...),
		predicates:  append([]predicate.ActivityValues{}, avq.predicates...),
		withMonster: avq.withMonster.Clone(),
		// clone intermediate query.
		sql:  avq.sql.Clone(),
		path: avq.path,
	}
}

// WithMonster tells the query-builder to eager-load the nodes that are connected to
// the "monster" edge. The optional arguments are used to configure the query builder of the edge.
func (avq *ActivityValuesQuery) WithMonster(opts ...func(*MonsterQuery)) *ActivityValuesQuery {
	query := &MonsterQuery{config: avq.config}
	for _, opt := range opts {
		opt(query)
	}
	avq.withMonster = query
	return avq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		Activity int `json:"activity,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.ActivityValues.Query().
//		GroupBy(activityvalues.FieldActivity).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
//
func (avq *ActivityValuesQuery) GroupBy(field string, fields ...string) *ActivityValuesGroupBy {
	group := &ActivityValuesGroupBy{config: avq.config}
	group.fields = append([]string{field}, fields...)
	group.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := avq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return avq.sqlQuery(ctx), nil
	}
	return group
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		Activity int `json:"activity,omitempty"`
//	}
//
//	client.ActivityValues.Query().
//		Select(activityvalues.FieldActivity).
//		Scan(ctx, &v)
//
func (avq *ActivityValuesQuery) Select(field string, fields ...string) *ActivityValuesSelect {
	avq.fields = append([]string{field}, fields...)
	return &ActivityValuesSelect{ActivityValuesQuery: avq}
}

func (avq *ActivityValuesQuery) prepareQuery(ctx context.Context) error {
	for _, f := range avq.fields {
		if !activityvalues.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if avq.path != nil {
		prev, err := avq.path(ctx)
		if err != nil {
			return err
		}
		avq.sql = prev
	}
	return nil
}

func (avq *ActivityValuesQuery) sqlAll(ctx context.Context) ([]*ActivityValues, error) {
	var (
		nodes       = []*ActivityValues{}
		_spec       = avq.querySpec()
		loadedTypes = [1]bool{
			avq.withMonster != nil,
		}
	)
	_spec.ScanValues = func(columns []string) ([]interface{}, error) {
		node := &ActivityValues{config: avq.config}
		nodes = append(nodes, node)
		return node.scanValues(columns)
	}
	_spec.Assign = func(columns []string, values []interface{}) error {
		if len(nodes) == 0 {
			return fmt.Errorf("ent: Assign called without calling ScanValues")
		}
		node := nodes[len(nodes)-1]
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	if err := sqlgraph.QueryNodes(ctx, avq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}

	if query := avq.withMonster; query != nil {
		fks := make([]driver.Value, 0, len(nodes))
		nodeids := make(map[int]*ActivityValues)
		for i := range nodes {
			fks = append(fks, nodes[i].ID)
			nodeids[nodes[i].ID] = nodes[i]
			nodes[i].Edges.Monster = []*Monster{}
		}
		query.withFKs = true
		query.Where(predicate.Monster(func(s *sql.Selector) {
			s.Where(sql.InValues(activityvalues.MonsterColumn, fks...))
		}))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			fk := n.monster_activity_values
			if fk == nil {
				return nil, fmt.Errorf(`foreign-key "monster_activity_values" is nil for node %v`, n.ID)
			}
			node, ok := nodeids[*fk]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "monster_activity_values" returned %v for node %v`, *fk, n.ID)
			}
			node.Edges.Monster = append(node.Edges.Monster, n)
		}
	}

	return nodes, nil
}

func (avq *ActivityValuesQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := avq.querySpec()
	return sqlgraph.CountNodes(ctx, avq.driver, _spec)
}

func (avq *ActivityValuesQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := avq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %w", err)
	}
	return n > 0, nil
}

func (avq *ActivityValuesQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   activityvalues.Table,
			Columns: activityvalues.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: activityvalues.FieldID,
			},
		},
		From:   avq.sql,
		Unique: true,
	}
	if unique := avq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := avq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, activityvalues.FieldID)
		for i := range fields {
			if fields[i] != activityvalues.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := avq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := avq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := avq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := avq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (avq *ActivityValuesQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(avq.driver.Dialect())
	t1 := builder.Table(activityvalues.Table)
	selector := builder.Select(t1.Columns(activityvalues.Columns...)...).From(t1)
	if avq.sql != nil {
		selector = avq.sql
		selector.Select(selector.Columns(activityvalues.Columns...)...)
	}
	for _, p := range avq.predicates {
		p(selector)
	}
	for _, p := range avq.order {
		p(selector)
	}
	if offset := avq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := avq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// ActivityValuesGroupBy is the group-by builder for ActivityValues entities.
type ActivityValuesGroupBy struct {
	config
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (avgb *ActivityValuesGroupBy) Aggregate(fns ...AggregateFunc) *ActivityValuesGroupBy {
	avgb.fns = append(avgb.fns, fns...)
	return avgb
}

// Scan applies the group-by query and scans the result into the given value.
func (avgb *ActivityValuesGroupBy) Scan(ctx context.Context, v interface{}) error {
	query, err := avgb.path(ctx)
	if err != nil {
		return err
	}
	avgb.sql = query
	return avgb.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) ScanX(ctx context.Context, v interface{}) {
	if err := avgb.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from group-by.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Strings(ctx context.Context) ([]string, error) {
	if len(avgb.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesGroupBy.Strings is not achievable when grouping more than 1 field")
	}
	var v []string
	if err := avgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) StringsX(ctx context.Context) []string {
	v, err := avgb.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = avgb.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesGroupBy.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) StringX(ctx context.Context) string {
	v, err := avgb.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from group-by.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Ints(ctx context.Context) ([]int, error) {
	if len(avgb.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesGroupBy.Ints is not achievable when grouping more than 1 field")
	}
	var v []int
	if err := avgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) IntsX(ctx context.Context) []int {
	v, err := avgb.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = avgb.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesGroupBy.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) IntX(ctx context.Context) int {
	v, err := avgb.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from group-by.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Float64s(ctx context.Context) ([]float64, error) {
	if len(avgb.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesGroupBy.Float64s is not achievable when grouping more than 1 field")
	}
	var v []float64
	if err := avgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) Float64sX(ctx context.Context) []float64 {
	v, err := avgb.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = avgb.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesGroupBy.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) Float64X(ctx context.Context) float64 {
	v, err := avgb.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from group-by.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Bools(ctx context.Context) ([]bool, error) {
	if len(avgb.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesGroupBy.Bools is not achievable when grouping more than 1 field")
	}
	var v []bool
	if err := avgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) BoolsX(ctx context.Context) []bool {
	v, err := avgb.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (avgb *ActivityValuesGroupBy) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = avgb.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesGroupBy.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (avgb *ActivityValuesGroupBy) BoolX(ctx context.Context) bool {
	v, err := avgb.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (avgb *ActivityValuesGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	for _, f := range avgb.fields {
		if !activityvalues.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := avgb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := avgb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (avgb *ActivityValuesGroupBy) sqlQuery() *sql.Selector {
	selector := avgb.sql
	columns := make([]string, 0, len(avgb.fields)+len(avgb.fns))
	columns = append(columns, avgb.fields...)
	for _, fn := range avgb.fns {
		columns = append(columns, fn(selector))
	}
	return selector.Select(columns...).GroupBy(avgb.fields...)
}

// ActivityValuesSelect is the builder for selecting fields of ActivityValues entities.
type ActivityValuesSelect struct {
	*ActivityValuesQuery
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Scan applies the selector query and scans the result into the given value.
func (avs *ActivityValuesSelect) Scan(ctx context.Context, v interface{}) error {
	if err := avs.prepareQuery(ctx); err != nil {
		return err
	}
	avs.sql = avs.ActivityValuesQuery.sqlQuery(ctx)
	return avs.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (avs *ActivityValuesSelect) ScanX(ctx context.Context, v interface{}) {
	if err := avs.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Strings(ctx context.Context) ([]string, error) {
	if len(avs.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesSelect.Strings is not achievable when selecting more than 1 field")
	}
	var v []string
	if err := avs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (avs *ActivityValuesSelect) StringsX(ctx context.Context) []string {
	v, err := avs.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = avs.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesSelect.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (avs *ActivityValuesSelect) StringX(ctx context.Context) string {
	v, err := avs.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Ints(ctx context.Context) ([]int, error) {
	if len(avs.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesSelect.Ints is not achievable when selecting more than 1 field")
	}
	var v []int
	if err := avs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (avs *ActivityValuesSelect) IntsX(ctx context.Context) []int {
	v, err := avs.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = avs.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesSelect.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (avs *ActivityValuesSelect) IntX(ctx context.Context) int {
	v, err := avs.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Float64s(ctx context.Context) ([]float64, error) {
	if len(avs.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesSelect.Float64s is not achievable when selecting more than 1 field")
	}
	var v []float64
	if err := avs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (avs *ActivityValuesSelect) Float64sX(ctx context.Context) []float64 {
	v, err := avs.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = avs.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesSelect.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (avs *ActivityValuesSelect) Float64X(ctx context.Context) float64 {
	v, err := avs.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Bools(ctx context.Context) ([]bool, error) {
	if len(avs.fields) > 1 {
		return nil, errors.New("ent: ActivityValuesSelect.Bools is not achievable when selecting more than 1 field")
	}
	var v []bool
	if err := avs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (avs *ActivityValuesSelect) BoolsX(ctx context.Context) []bool {
	v, err := avs.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a selector. It is only allowed when selecting one field.
func (avs *ActivityValuesSelect) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = avs.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{activityvalues.Label}
	default:
		err = fmt.Errorf("ent: ActivityValuesSelect.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (avs *ActivityValuesSelect) BoolX(ctx context.Context) bool {
	v, err := avs.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (avs *ActivityValuesSelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := avs.sqlQuery().Query()
	if err := avs.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (avs *ActivityValuesSelect) sqlQuery() sql.Querier {
	selector := avs.sql
	selector.Select(selector.Columns(avs.fields...)...)
	return selector
}
