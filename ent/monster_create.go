// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/vajar_minigame/minigame_backend/ent/activityvalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/battlevalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/bodyvalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/monster"
	"gitlab.com/vajar_minigame/minigame_backend/ent/statusvalues"
)

// MonsterCreate is the builder for creating a Monster entity.
type MonsterCreate struct {
	config
	mutation *MonsterMutation
	hooks    []Hook
}

// SetUserID sets the "user_id" field.
func (mc *MonsterCreate) SetUserID(i int) *MonsterCreate {
	mc.mutation.SetUserID(i)
	return mc
}

// SetName sets the "name" field.
func (mc *MonsterCreate) SetName(s string) *MonsterCreate {
	mc.mutation.SetName(s)
	return mc
}

// SetLastUpdated sets the "last_updated" field.
func (mc *MonsterCreate) SetLastUpdated(t time.Time) *MonsterCreate {
	mc.mutation.SetLastUpdated(t)
	return mc
}

// SetNillableLastUpdated sets the "last_updated" field if the given value is not nil.
func (mc *MonsterCreate) SetNillableLastUpdated(t *time.Time) *MonsterCreate {
	if t != nil {
		mc.SetLastUpdated(*t)
	}
	return mc
}

// SetIsTest sets the "is_test" field.
func (mc *MonsterCreate) SetIsTest(b bool) *MonsterCreate {
	mc.mutation.SetIsTest(b)
	return mc
}

// SetID sets the "id" field.
func (mc *MonsterCreate) SetID(i int) *MonsterCreate {
	mc.mutation.SetID(i)
	return mc
}

// SetBattleValuesID sets the "battle_values" edge to the BattleValues entity by ID.
func (mc *MonsterCreate) SetBattleValuesID(id int) *MonsterCreate {
	mc.mutation.SetBattleValuesID(id)
	return mc
}

// SetNillableBattleValuesID sets the "battle_values" edge to the BattleValues entity by ID if the given value is not nil.
func (mc *MonsterCreate) SetNillableBattleValuesID(id *int) *MonsterCreate {
	if id != nil {
		mc = mc.SetBattleValuesID(*id)
	}
	return mc
}

// SetBattleValues sets the "battle_values" edge to the BattleValues entity.
func (mc *MonsterCreate) SetBattleValues(b *BattleValues) *MonsterCreate {
	return mc.SetBattleValuesID(b.ID)
}

// SetBodyValuesID sets the "body_values" edge to the BodyValues entity by ID.
func (mc *MonsterCreate) SetBodyValuesID(id int) *MonsterCreate {
	mc.mutation.SetBodyValuesID(id)
	return mc
}

// SetNillableBodyValuesID sets the "body_values" edge to the BodyValues entity by ID if the given value is not nil.
func (mc *MonsterCreate) SetNillableBodyValuesID(id *int) *MonsterCreate {
	if id != nil {
		mc = mc.SetBodyValuesID(*id)
	}
	return mc
}

// SetBodyValues sets the "body_values" edge to the BodyValues entity.
func (mc *MonsterCreate) SetBodyValues(b *BodyValues) *MonsterCreate {
	return mc.SetBodyValuesID(b.ID)
}

// SetActivityValuesID sets the "activity_values" edge to the ActivityValues entity by ID.
func (mc *MonsterCreate) SetActivityValuesID(id int) *MonsterCreate {
	mc.mutation.SetActivityValuesID(id)
	return mc
}

// SetNillableActivityValuesID sets the "activity_values" edge to the ActivityValues entity by ID if the given value is not nil.
func (mc *MonsterCreate) SetNillableActivityValuesID(id *int) *MonsterCreate {
	if id != nil {
		mc = mc.SetActivityValuesID(*id)
	}
	return mc
}

// SetActivityValues sets the "activity_values" edge to the ActivityValues entity.
func (mc *MonsterCreate) SetActivityValues(a *ActivityValues) *MonsterCreate {
	return mc.SetActivityValuesID(a.ID)
}

// SetStatusValuesID sets the "status_values" edge to the StatusValues entity by ID.
func (mc *MonsterCreate) SetStatusValuesID(id int) *MonsterCreate {
	mc.mutation.SetStatusValuesID(id)
	return mc
}

// SetNillableStatusValuesID sets the "status_values" edge to the StatusValues entity by ID if the given value is not nil.
func (mc *MonsterCreate) SetNillableStatusValuesID(id *int) *MonsterCreate {
	if id != nil {
		mc = mc.SetStatusValuesID(*id)
	}
	return mc
}

// SetStatusValues sets the "status_values" edge to the StatusValues entity.
func (mc *MonsterCreate) SetStatusValues(s *StatusValues) *MonsterCreate {
	return mc.SetStatusValuesID(s.ID)
}

// Mutation returns the MonsterMutation object of the builder.
func (mc *MonsterCreate) Mutation() *MonsterMutation {
	return mc.mutation
}

// Save creates the Monster in the database.
func (mc *MonsterCreate) Save(ctx context.Context) (*Monster, error) {
	var (
		err  error
		node *Monster
	)
	mc.defaults()
	if len(mc.hooks) == 0 {
		if err = mc.check(); err != nil {
			return nil, err
		}
		node, err = mc.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*MonsterMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = mc.check(); err != nil {
				return nil, err
			}
			mc.mutation = mutation
			node, err = mc.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(mc.hooks) - 1; i >= 0; i-- {
			mut = mc.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, mc.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (mc *MonsterCreate) SaveX(ctx context.Context) *Monster {
	v, err := mc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// defaults sets the default values of the builder before save.
func (mc *MonsterCreate) defaults() {
	if _, ok := mc.mutation.LastUpdated(); !ok {
		v := monster.DefaultLastUpdated()
		mc.mutation.SetLastUpdated(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (mc *MonsterCreate) check() error {
	if _, ok := mc.mutation.UserID(); !ok {
		return &ValidationError{Name: "user_id", err: errors.New("ent: missing required field \"user_id\"")}
	}
	if _, ok := mc.mutation.Name(); !ok {
		return &ValidationError{Name: "name", err: errors.New("ent: missing required field \"name\"")}
	}
	if _, ok := mc.mutation.LastUpdated(); !ok {
		return &ValidationError{Name: "last_updated", err: errors.New("ent: missing required field \"last_updated\"")}
	}
	if _, ok := mc.mutation.IsTest(); !ok {
		return &ValidationError{Name: "is_test", err: errors.New("ent: missing required field \"is_test\"")}
	}
	return nil
}

func (mc *MonsterCreate) sqlSave(ctx context.Context) (*Monster, error) {
	_node, _spec := mc.createSpec()
	if err := sqlgraph.CreateNode(ctx, mc.driver, _spec); err != nil {
		if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	if _node.ID == 0 {
		id := _spec.ID.Value.(int64)
		_node.ID = int(id)
	}
	return _node, nil
}

func (mc *MonsterCreate) createSpec() (*Monster, *sqlgraph.CreateSpec) {
	var (
		_node = &Monster{config: mc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: monster.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: monster.FieldID,
			},
		}
	)
	if id, ok := mc.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = id
	}
	if value, ok := mc.mutation.UserID(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  value,
			Column: monster.FieldUserID,
		})
		_node.UserID = value
	}
	if value, ok := mc.mutation.Name(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: monster.FieldName,
		})
		_node.Name = value
	}
	if value, ok := mc.mutation.LastUpdated(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: monster.FieldLastUpdated,
		})
		_node.LastUpdated = value
	}
	if value, ok := mc.mutation.IsTest(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeBool,
			Value:  value,
			Column: monster.FieldIsTest,
		})
		_node.IsTest = value
	}
	if nodes := mc.mutation.BattleValuesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: false,
			Table:   monster.BattleValuesTable,
			Columns: []string{monster.BattleValuesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: battlevalues.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.monster_battle_values = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := mc.mutation.BodyValuesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: false,
			Table:   monster.BodyValuesTable,
			Columns: []string{monster.BodyValuesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: bodyvalues.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.monster_body_values = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := mc.mutation.ActivityValuesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: false,
			Table:   monster.ActivityValuesTable,
			Columns: []string{monster.ActivityValuesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: activityvalues.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.monster_activity_values = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := mc.mutation.StatusValuesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: false,
			Table:   monster.StatusValuesTable,
			Columns: []string{monster.StatusValuesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: statusvalues.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.monster_status_values = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// MonsterCreateBulk is the builder for creating many Monster entities in bulk.
type MonsterCreateBulk struct {
	config
	builders []*MonsterCreate
}

// Save creates the Monster entities in the database.
func (mcb *MonsterCreateBulk) Save(ctx context.Context) ([]*Monster, error) {
	specs := make([]*sqlgraph.CreateSpec, len(mcb.builders))
	nodes := make([]*Monster, len(mcb.builders))
	mutators := make([]Mutator, len(mcb.builders))
	for i := range mcb.builders {
		func(i int, root context.Context) {
			builder := mcb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*MonsterMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, mcb.builders[i+1].mutation)
				} else {
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, mcb.driver, &sqlgraph.BatchCreateSpec{Nodes: specs}); err != nil {
						if cerr, ok := isSQLConstraintError(err); ok {
							err = cerr
						}
					}
				}
				mutation.done = true
				if err != nil {
					return nil, err
				}
				if nodes[i].ID == 0 {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, mcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (mcb *MonsterCreateBulk) SaveX(ctx context.Context) []*Monster {
	v, err := mcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}
