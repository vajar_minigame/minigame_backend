// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"database/sql/driver"
	"errors"
	"fmt"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/vajar_minigame/minigame_backend/ent/monster"
	"gitlab.com/vajar_minigame/minigame_backend/ent/predicate"
	"gitlab.com/vajar_minigame/minigame_backend/ent/statusvalues"
)

// StatusValuesQuery is the builder for querying StatusValues entities.
type StatusValuesQuery struct {
	config
	limit      *int
	offset     *int
	unique     *bool
	order      []OrderFunc
	fields     []string
	predicates []predicate.StatusValues
	// eager-loading edges.
	withMonster *MonsterQuery
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the StatusValuesQuery builder.
func (svq *StatusValuesQuery) Where(ps ...predicate.StatusValues) *StatusValuesQuery {
	svq.predicates = append(svq.predicates, ps...)
	return svq
}

// Limit adds a limit step to the query.
func (svq *StatusValuesQuery) Limit(limit int) *StatusValuesQuery {
	svq.limit = &limit
	return svq
}

// Offset adds an offset step to the query.
func (svq *StatusValuesQuery) Offset(offset int) *StatusValuesQuery {
	svq.offset = &offset
	return svq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (svq *StatusValuesQuery) Unique(unique bool) *StatusValuesQuery {
	svq.unique = &unique
	return svq
}

// Order adds an order step to the query.
func (svq *StatusValuesQuery) Order(o ...OrderFunc) *StatusValuesQuery {
	svq.order = append(svq.order, o...)
	return svq
}

// QueryMonster chains the current query on the "monster" edge.
func (svq *StatusValuesQuery) QueryMonster() *MonsterQuery {
	query := &MonsterQuery{config: svq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := svq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := svq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(statusvalues.Table, statusvalues.FieldID, selector),
			sqlgraph.To(monster.Table, monster.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, true, statusvalues.MonsterTable, statusvalues.MonsterColumn),
		)
		fromU = sqlgraph.SetNeighbors(svq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first StatusValues entity from the query.
// Returns a *NotFoundError when no StatusValues was found.
func (svq *StatusValuesQuery) First(ctx context.Context) (*StatusValues, error) {
	nodes, err := svq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{statusvalues.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (svq *StatusValuesQuery) FirstX(ctx context.Context) *StatusValues {
	node, err := svq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first StatusValues ID from the query.
// Returns a *NotFoundError when no StatusValues ID was found.
func (svq *StatusValuesQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = svq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{statusvalues.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (svq *StatusValuesQuery) FirstIDX(ctx context.Context) int {
	id, err := svq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single StatusValues entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when exactly one StatusValues entity is not found.
// Returns a *NotFoundError when no StatusValues entities are found.
func (svq *StatusValuesQuery) Only(ctx context.Context) (*StatusValues, error) {
	nodes, err := svq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{statusvalues.Label}
	default:
		return nil, &NotSingularError{statusvalues.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (svq *StatusValuesQuery) OnlyX(ctx context.Context) *StatusValues {
	node, err := svq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only StatusValues ID in the query.
// Returns a *NotSingularError when exactly one StatusValues ID is not found.
// Returns a *NotFoundError when no entities are found.
func (svq *StatusValuesQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = svq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = &NotSingularError{statusvalues.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (svq *StatusValuesQuery) OnlyIDX(ctx context.Context) int {
	id, err := svq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of StatusValuesSlice.
func (svq *StatusValuesQuery) All(ctx context.Context) ([]*StatusValues, error) {
	if err := svq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return svq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (svq *StatusValuesQuery) AllX(ctx context.Context) []*StatusValues {
	nodes, err := svq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of StatusValues IDs.
func (svq *StatusValuesQuery) IDs(ctx context.Context) ([]int, error) {
	var ids []int
	if err := svq.Select(statusvalues.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (svq *StatusValuesQuery) IDsX(ctx context.Context) []int {
	ids, err := svq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (svq *StatusValuesQuery) Count(ctx context.Context) (int, error) {
	if err := svq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return svq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (svq *StatusValuesQuery) CountX(ctx context.Context) int {
	count, err := svq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (svq *StatusValuesQuery) Exist(ctx context.Context) (bool, error) {
	if err := svq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return svq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (svq *StatusValuesQuery) ExistX(ctx context.Context) bool {
	exist, err := svq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the StatusValuesQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (svq *StatusValuesQuery) Clone() *StatusValuesQuery {
	if svq == nil {
		return nil
	}
	return &StatusValuesQuery{
		config:      svq.config,
		limit:       svq.limit,
		offset:      svq.offset,
		order:       append([]OrderFunc{}, svq.order...),
		predicates:  append([]predicate.StatusValues{}, svq.predicates...),
		withMonster: svq.withMonster.Clone(),
		// clone intermediate query.
		sql:  svq.sql.Clone(),
		path: svq.path,
	}
}

// WithMonster tells the query-builder to eager-load the nodes that are connected to
// the "monster" edge. The optional arguments are used to configure the query builder of the edge.
func (svq *StatusValuesQuery) WithMonster(opts ...func(*MonsterQuery)) *StatusValuesQuery {
	query := &MonsterQuery{config: svq.config}
	for _, opt := range opts {
		opt(query)
	}
	svq.withMonster = query
	return svq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		IsAlive bool `json:"is_alive,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.StatusValues.Query().
//		GroupBy(statusvalues.FieldIsAlive).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
//
func (svq *StatusValuesQuery) GroupBy(field string, fields ...string) *StatusValuesGroupBy {
	group := &StatusValuesGroupBy{config: svq.config}
	group.fields = append([]string{field}, fields...)
	group.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := svq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return svq.sqlQuery(ctx), nil
	}
	return group
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		IsAlive bool `json:"is_alive,omitempty"`
//	}
//
//	client.StatusValues.Query().
//		Select(statusvalues.FieldIsAlive).
//		Scan(ctx, &v)
//
func (svq *StatusValuesQuery) Select(field string, fields ...string) *StatusValuesSelect {
	svq.fields = append([]string{field}, fields...)
	return &StatusValuesSelect{StatusValuesQuery: svq}
}

func (svq *StatusValuesQuery) prepareQuery(ctx context.Context) error {
	for _, f := range svq.fields {
		if !statusvalues.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if svq.path != nil {
		prev, err := svq.path(ctx)
		if err != nil {
			return err
		}
		svq.sql = prev
	}
	return nil
}

func (svq *StatusValuesQuery) sqlAll(ctx context.Context) ([]*StatusValues, error) {
	var (
		nodes       = []*StatusValues{}
		_spec       = svq.querySpec()
		loadedTypes = [1]bool{
			svq.withMonster != nil,
		}
	)
	_spec.ScanValues = func(columns []string) ([]interface{}, error) {
		node := &StatusValues{config: svq.config}
		nodes = append(nodes, node)
		return node.scanValues(columns)
	}
	_spec.Assign = func(columns []string, values []interface{}) error {
		if len(nodes) == 0 {
			return fmt.Errorf("ent: Assign called without calling ScanValues")
		}
		node := nodes[len(nodes)-1]
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	if err := sqlgraph.QueryNodes(ctx, svq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}

	if query := svq.withMonster; query != nil {
		fks := make([]driver.Value, 0, len(nodes))
		nodeids := make(map[int]*StatusValues)
		for i := range nodes {
			fks = append(fks, nodes[i].ID)
			nodeids[nodes[i].ID] = nodes[i]
			nodes[i].Edges.Monster = []*Monster{}
		}
		query.withFKs = true
		query.Where(predicate.Monster(func(s *sql.Selector) {
			s.Where(sql.InValues(statusvalues.MonsterColumn, fks...))
		}))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			fk := n.monster_status_values
			if fk == nil {
				return nil, fmt.Errorf(`foreign-key "monster_status_values" is nil for node %v`, n.ID)
			}
			node, ok := nodeids[*fk]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "monster_status_values" returned %v for node %v`, *fk, n.ID)
			}
			node.Edges.Monster = append(node.Edges.Monster, n)
		}
	}

	return nodes, nil
}

func (svq *StatusValuesQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := svq.querySpec()
	return sqlgraph.CountNodes(ctx, svq.driver, _spec)
}

func (svq *StatusValuesQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := svq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %w", err)
	}
	return n > 0, nil
}

func (svq *StatusValuesQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   statusvalues.Table,
			Columns: statusvalues.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: statusvalues.FieldID,
			},
		},
		From:   svq.sql,
		Unique: true,
	}
	if unique := svq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := svq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, statusvalues.FieldID)
		for i := range fields {
			if fields[i] != statusvalues.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := svq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := svq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := svq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := svq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (svq *StatusValuesQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(svq.driver.Dialect())
	t1 := builder.Table(statusvalues.Table)
	selector := builder.Select(t1.Columns(statusvalues.Columns...)...).From(t1)
	if svq.sql != nil {
		selector = svq.sql
		selector.Select(selector.Columns(statusvalues.Columns...)...)
	}
	for _, p := range svq.predicates {
		p(selector)
	}
	for _, p := range svq.order {
		p(selector)
	}
	if offset := svq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := svq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// StatusValuesGroupBy is the group-by builder for StatusValues entities.
type StatusValuesGroupBy struct {
	config
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (svgb *StatusValuesGroupBy) Aggregate(fns ...AggregateFunc) *StatusValuesGroupBy {
	svgb.fns = append(svgb.fns, fns...)
	return svgb
}

// Scan applies the group-by query and scans the result into the given value.
func (svgb *StatusValuesGroupBy) Scan(ctx context.Context, v interface{}) error {
	query, err := svgb.path(ctx)
	if err != nil {
		return err
	}
	svgb.sql = query
	return svgb.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) ScanX(ctx context.Context, v interface{}) {
	if err := svgb.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from group-by.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Strings(ctx context.Context) ([]string, error) {
	if len(svgb.fields) > 1 {
		return nil, errors.New("ent: StatusValuesGroupBy.Strings is not achievable when grouping more than 1 field")
	}
	var v []string
	if err := svgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) StringsX(ctx context.Context) []string {
	v, err := svgb.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = svgb.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesGroupBy.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) StringX(ctx context.Context) string {
	v, err := svgb.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from group-by.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Ints(ctx context.Context) ([]int, error) {
	if len(svgb.fields) > 1 {
		return nil, errors.New("ent: StatusValuesGroupBy.Ints is not achievable when grouping more than 1 field")
	}
	var v []int
	if err := svgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) IntsX(ctx context.Context) []int {
	v, err := svgb.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = svgb.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesGroupBy.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) IntX(ctx context.Context) int {
	v, err := svgb.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from group-by.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Float64s(ctx context.Context) ([]float64, error) {
	if len(svgb.fields) > 1 {
		return nil, errors.New("ent: StatusValuesGroupBy.Float64s is not achievable when grouping more than 1 field")
	}
	var v []float64
	if err := svgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) Float64sX(ctx context.Context) []float64 {
	v, err := svgb.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = svgb.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesGroupBy.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) Float64X(ctx context.Context) float64 {
	v, err := svgb.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from group-by.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Bools(ctx context.Context) ([]bool, error) {
	if len(svgb.fields) > 1 {
		return nil, errors.New("ent: StatusValuesGroupBy.Bools is not achievable when grouping more than 1 field")
	}
	var v []bool
	if err := svgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) BoolsX(ctx context.Context) []bool {
	v, err := svgb.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (svgb *StatusValuesGroupBy) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = svgb.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesGroupBy.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (svgb *StatusValuesGroupBy) BoolX(ctx context.Context) bool {
	v, err := svgb.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (svgb *StatusValuesGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	for _, f := range svgb.fields {
		if !statusvalues.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := svgb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := svgb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (svgb *StatusValuesGroupBy) sqlQuery() *sql.Selector {
	selector := svgb.sql
	columns := make([]string, 0, len(svgb.fields)+len(svgb.fns))
	columns = append(columns, svgb.fields...)
	for _, fn := range svgb.fns {
		columns = append(columns, fn(selector))
	}
	return selector.Select(columns...).GroupBy(svgb.fields...)
}

// StatusValuesSelect is the builder for selecting fields of StatusValues entities.
type StatusValuesSelect struct {
	*StatusValuesQuery
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Scan applies the selector query and scans the result into the given value.
func (svs *StatusValuesSelect) Scan(ctx context.Context, v interface{}) error {
	if err := svs.prepareQuery(ctx); err != nil {
		return err
	}
	svs.sql = svs.StatusValuesQuery.sqlQuery(ctx)
	return svs.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (svs *StatusValuesSelect) ScanX(ctx context.Context, v interface{}) {
	if err := svs.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Strings(ctx context.Context) ([]string, error) {
	if len(svs.fields) > 1 {
		return nil, errors.New("ent: StatusValuesSelect.Strings is not achievable when selecting more than 1 field")
	}
	var v []string
	if err := svs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (svs *StatusValuesSelect) StringsX(ctx context.Context) []string {
	v, err := svs.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = svs.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesSelect.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (svs *StatusValuesSelect) StringX(ctx context.Context) string {
	v, err := svs.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Ints(ctx context.Context) ([]int, error) {
	if len(svs.fields) > 1 {
		return nil, errors.New("ent: StatusValuesSelect.Ints is not achievable when selecting more than 1 field")
	}
	var v []int
	if err := svs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (svs *StatusValuesSelect) IntsX(ctx context.Context) []int {
	v, err := svs.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = svs.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesSelect.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (svs *StatusValuesSelect) IntX(ctx context.Context) int {
	v, err := svs.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Float64s(ctx context.Context) ([]float64, error) {
	if len(svs.fields) > 1 {
		return nil, errors.New("ent: StatusValuesSelect.Float64s is not achievable when selecting more than 1 field")
	}
	var v []float64
	if err := svs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (svs *StatusValuesSelect) Float64sX(ctx context.Context) []float64 {
	v, err := svs.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = svs.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesSelect.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (svs *StatusValuesSelect) Float64X(ctx context.Context) float64 {
	v, err := svs.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Bools(ctx context.Context) ([]bool, error) {
	if len(svs.fields) > 1 {
		return nil, errors.New("ent: StatusValuesSelect.Bools is not achievable when selecting more than 1 field")
	}
	var v []bool
	if err := svs.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (svs *StatusValuesSelect) BoolsX(ctx context.Context) []bool {
	v, err := svs.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a selector. It is only allowed when selecting one field.
func (svs *StatusValuesSelect) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = svs.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{statusvalues.Label}
	default:
		err = fmt.Errorf("ent: StatusValuesSelect.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (svs *StatusValuesSelect) BoolX(ctx context.Context) bool {
	v, err := svs.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (svs *StatusValuesSelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := svs.sqlQuery().Query()
	if err := svs.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (svs *StatusValuesSelect) sqlQuery() sql.Querier {
	selector := svs.sql
	selector.Select(selector.Columns(svs.fields...)...)
	return selector
}
