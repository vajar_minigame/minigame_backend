package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the Monster entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Immutable().Unique(),
		field.String("name"),
		field.Time("last_update").Default(time.Now().UTC).UpdateDefault(time.Now().UTC),
		field.Bool("is_test"),
	}
}
