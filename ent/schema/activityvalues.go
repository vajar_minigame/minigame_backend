package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// ActivityValues holds the schema definition for the ActivityValues entity.
type ActivityValues struct {
	ent.Schema
}

// Fields of the ActivityValues.
func (ActivityValues) Fields() []ent.Field {
	return []ent.Field{
		field.Int("activity"),
		field.Int("activity_id"),
	}
}

// Edges of the ActivityValues.
func (ActivityValues) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("monster", Monster.Type).Ref("activity_values"),
	}
}
