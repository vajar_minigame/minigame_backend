package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// BattleValues holds the schema definition for the BattleValues entity.
type BattleValues struct {
	ent.Schema
}

// Fields of the BattleValues.
func (BattleValues) Fields() []ent.Field {
	return []ent.Field{
		field.Int("attack"),
		field.Int("defense"),
		field.Int("max_hp"),
		field.Int("remaining_hp"),
	}
}

// Edges of the BattleValues.
func (BattleValues) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("monster", Monster.Type).Ref("battle_values"),
	}
}
