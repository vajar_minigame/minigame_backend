package main

import (
	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/router"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
	logrus.SetOutput(colorable.NewColorableStdout())
	logrus.SetLevel(logrus.TraceLevel)

	router.StartServer()
}
