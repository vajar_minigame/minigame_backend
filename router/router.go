package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/clientsession"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/controller"

	"gitlab.com/vajar_minigame/minigame_backend/storage"

	dapr "github.com/dapr/go-sdk/client"
	daprd "github.com/dapr/go-sdk/service/http"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/config"
)

type MonsterController interface {
	GetMonByID(w http.ResponseWriter, r *http.Request)
	UpdateMon(w http.ResponseWriter, r *http.Request)
}

type UserController interface {
	GetUserByID(w http.ResponseWriter, r *http.Request)
	AddUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
}

type server struct {
	db     dbs
	config *config.Config
}

type dbs struct {
	storage.MonsterDB
}

const address = "localhost:5002"

func startDaprPublisher(config *config.Config) clientsession.EventPublisher {
	client, err := dapr.NewClient()
	if err != nil {
		logrus.Fatal("cant connect to client", err)
	}
	return clientsession.NewPublishClient(client)
}

func CreateMonRouter(s *mux.Router, monController MonsterController) *mux.Router {

	s.HandleFunc("{id:[0-9]+}", monController.GetMonByID).Methods("GET")
	return s
}

func CreateUserRouter(s *mux.Router, userController UserController) *mux.Router {

	s.HandleFunc("{id:[0-9]+}", userController.GetUserByID).Methods("GET")
	return s
}

func StartServer() {
	config := config.LoadConfig("resources/config")

	logrus.SetLevel(logrus.TraceLevel)
	logrus.SetReportCaller(true)

	monDB := storage.NewDatabases(config)

	storage.BootstrapDBs(monDB)

	logrus.Info("Connection to DB established")

	dapr := startDaprPublisher(config)

	monController := controller.NewMonController(config, monDB, dapr)

	mr := mux.NewRouter()
	r := mr.PathPrefix("/api/v1.0/").Subrouter()
	r.HandleFunc("/monster", monController.AddMon).Methods(http.MethodPost)
	r.HandleFunc("/monster/{id:[0-9]+}", monController.GetMonByID).Methods(http.MethodGet)
	r.HandleFunc("/monster/{id:[0-9]+}", monController.UpdateMon).Methods(http.MethodPut)
	r.HandleFunc("/monster", monController.GetMonstersByQuery).Methods(http.MethodGet)

	mux := http.NewServeMux()
	mux.Handle("/api/", r)

	daprService := daprd.NewServiceWithMux(address, mux)
	logrus.Fatal(daprService.Start())

}
