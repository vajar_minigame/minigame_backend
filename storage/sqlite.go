package storage

import (
	"context"
	"log"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/vajar_minigame/minigame_backend/config"
	"gitlab.com/vajar_minigame/minigame_backend/ent"
	"gitlab.com/vajar_minigame/minigame_backend/storage/repository"
)

type SqliteDB struct {
	client *ent.Client
}

func NewSqliteDB() (MonsterDB, error) {

	client, err := ent.Open("sqlite3", "file:ents.db?&cache=shared&_fk=1")
	if err != nil {
		log.Fatal(err)
	}

	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	monRepo := repository.NewMonsterRepo(client)

	return monRepo, nil
}

//NewDatabases creates all databases in one go
func NewDatabases(config *config.Config) MonsterDB {
	monDB, _ := NewSqliteDB()

	return monDB

}
