package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// StatusValues holds the schema definition for the StatusValues entity.
type StatusValues struct {
	ent.Schema
}

// Fields of the StatusValues.
func (StatusValues) Fields() []ent.Field {
	return []ent.Field{
		field.Bool("is_alive"),
	}
}

// Edges of the StatusValues.
func (StatusValues) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("monster", Monster.Type).Ref("status_values"),
	}
}
