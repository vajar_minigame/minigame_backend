package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// BodyValues holds the schema definition for the BodyValues entity.
type BodyValues struct {
	ent.Schema
}

// Fields of the BodyValues.
func (BodyValues) Fields() []ent.Field {
	return []ent.Field{
		field.Float("remaining_saturation"),
		field.Float("max_saturation"),
		field.Int("mass"),
	}
}

// Edges of the BodyValues.
func (BodyValues) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("monster", Monster.Type).Ref("body_values"),
	}
}
