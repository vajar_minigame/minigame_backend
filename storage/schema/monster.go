package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Monster holds the schema definition for the Monster entity.
type Monster struct {
	ent.Schema
}

// Fields of the Monster.
func (Monster) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Immutable().Unique(),
		field.Int("user_id"),
		field.String("name"),
		field.Time("last_updated").Default(time.Now().UTC).UpdateDefault(time.Now().UTC),
		field.Bool("is_test"),
	}
}

// Edges of the Monster.
func (Monster) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("battle_values", BattleValues.Type).Unique(),
		edge.To("body_values", BodyValues.Type).Unique(),
		edge.To("activity_values", ActivityValues.Type).Unique(),
		edge.To("status_values", StatusValues.Type).Unique(),
	}
}
