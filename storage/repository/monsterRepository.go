package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/ent"
	"gitlab.com/vajar_minigame/minigame_backend/ent/activityvalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/battlevalues"
	"gitlab.com/vajar_minigame/minigame_backend/ent/bodyvalues"
	entmon "gitlab.com/vajar_minigame/minigame_backend/ent/monster"
	"gitlab.com/vajar_minigame/minigame_backend/ent/statusvalues"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/data/metainfo"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/data/monstermodel"
)

type SqliteDB struct {
	client *ent.Client
}

func NewMonsterRepo(client *ent.Client) *SqliteDB {
	return &SqliteDB{client: client}
}

func (db *SqliteDB) AddMon(ctx context.Context, mon monstermodel.MonsterDto) (int, error) {

	var battleValues *ent.BattleValues
	var bodyValues *ent.BodyValues
	tx, err := db.client.BeginTx(ctx, &sql.TxOptions{})

	vals, err := tx.BattleValues.Create().
		SetAttack(mon.BattleValues.Attack).
		SetDefense(int(mon.BattleValues.Defense)).
		SetMaxHp(int(mon.BattleValues.MaxHp)).
		SetRemainingHp(int(mon.BattleValues.RemainingHp)).
		Save(ctx)
	if err != nil {
		logrus.Error("Error adding battleVals", err)
		return -1, err
	}
	battleValues = vals

	bVals, err2 := tx.BodyValues.Create().
		SetMass(int(mon.BodyValues.Mass)).
		SetMaxSaturation(mon.BodyValues.MaxSaturation).
		SetRemainingSaturation(mon.BodyValues.RemainingSaturation).
		Save(ctx)
	if err2 != nil {
		logrus.Error("Error adding battleVals", err)
		return -1, err
	}
	bodyValues = bVals

	status := updateStatusValues(mon.BattleValues)

	statusValues, err := tx.StatusValues.Create().
		SetIsAlive(status.IsAlive).
		Save(ctx)

	activityValues, err := tx.ActivityValues.Create().
		SetActivity(0).
		SetActivityID(0).
		Save(ctx)

	dbMon, err := tx.Monster.Create().SetName(mon.Name).
		SetUserID(int(mon.UserID)).
		SetBattleValues(battleValues).
		SetBodyValues(bodyValues).
		SetStatusValues(statusValues).
		SetActivityValues(activityValues).
		SetIsTest(mon.IsTest).
		Save(ctx)
	if err != nil {
		logrus.Error("Error adding stuff", err)
	}

	//db.statusValues[monID] = status
	tx.Commit()

	return dbMon.ID, nil

}

// todo business logic somewhere else; implication of updated values so should happen afterwards
func updateStatusValues(battleValues monstermodel.BattleValues) monstermodel.StatusValues {
	status := monstermodel.StatusValues{IsAlive: false}

	if battleValues.RemainingHp > 0 {
		status.IsAlive = true
	}
	return status

}

// GetMonByID partially fill every struct
//todo: change to one query
//save as json?
func (db *SqliteDB) GetMonByID(ctx context.Context, id int) (monstermodel.MonsterDto, error) {

	mon, err := db.client.Monster.Query().WithActivityValues().
		WithBattleValues().
		WithBodyValues().
		WithStatusValues().
		WithActivityValues().
		Where(entmon.ID(id)).
		First(ctx)
	if err != nil {
		return monstermodel.MonsterDto{}, errors.New("mon doesnt exist")
	}
	logrus.Info(mon)

	dto := monstermodel.MonsterDtoFromEnt(mon)

	return dto, nil
}

//GetMonsByUserID todo improve query
func (db *SqliteDB) GetMonsWithQuery(ctx context.Context, query monstermodel.MonsterQuery) ([]monstermodel.MonsterDto, metainfo.Pagination, error) {

	monQuery := db.client.Monster.Query().WithActivityValues().
		WithBattleValues().
		WithBodyValues().
		WithStatusValues()

	if query.UserID != 0 {
		monQuery = monQuery.Where(entmon.UserID(query.UserID))
	}

	//todo use offset when it works
	monList, err := monQuery.Limit(query.PageSize).Offset(0).All(ctx)
	if err != nil {
		return nil, metainfo.Pagination{}, err
	}

	mons := make([]monstermodel.MonsterDto, 0)

	// make([]T, len, cap) []T
	for _, mon := range monList {

		userMon := monstermodel.MonsterDtoFromEnt(mon)
		mons = append(mons, userMon)

	}
	tCount, err := monQuery.Count(ctx)
	if err != nil {
		return nil, metainfo.Pagination{}, err
	}
	pagination := metainfo.Pagination{PageSize: query.PageSize, PageIndex: query.PageIndex, PageTCount: tCount, PageCount: tCount/query.PageSize + 1}

	return mons, pagination, nil

}

// UpdateMon updates all values by the specified one, identifies by id
func (db *SqliteDB) UpdateMon(ctx context.Context, mon monstermodel.MonsterDto) error {

	tx, err := db.client.BeginTx(ctx, &sql.TxOptions{})

	_, err = tx.BattleValues.Update().
		Where(battlevalues.HasMonsterWith(entmon.ID(mon.ID))).
		SetAttack(int(mon.BattleValues.Attack)).
		SetDefense(int(mon.BattleValues.Defense)).
		SetMaxHp(int(mon.BattleValues.MaxHp)).
		SetRemainingHp(int(mon.BattleValues.RemainingHp)).
		Save(ctx)
	if err != nil {
		logrus.Error("Error adding battleVals", err)
		return err
	}

	_, err = tx.BodyValues.Update().
		Where(bodyvalues.HasMonsterWith(entmon.ID(mon.ID))).
		SetMass(int(mon.BodyValues.Mass)).
		SetMaxSaturation(mon.BodyValues.MaxSaturation).
		SetRemainingSaturation(mon.BodyValues.RemainingSaturation).
		Save(ctx)
	if err != nil {
		logrus.Error("Error adding battleVals", err)
		return err
	}

	status := updateStatusValues(mon.BattleValues)
	if err != nil {
		logrus.Error("Error adding battleVals", err)
		return err
	}

	_, err = tx.StatusValues.Update().
		Where(statusvalues.HasMonsterWith(entmon.ID(mon.ID))).
		SetIsAlive(status.IsAlive).
		Save(ctx)

	_, err = tx.Monster.Update().
		Where(entmon.ID(mon.ID)).
		SetName(mon.Name).
		SetUserID(int(mon.UserID)).
		SetIsTest(mon.IsTest).
		Save(ctx)
	if err != nil {
		logrus.Error("Error adding stuff", err)
	}

	//db.statusValues[monID] = status

	tx.Commit()
	return nil

}

func (db *SqliteDB) SetActivity(ctx context.Context, monID int, activity monstermodel.ActivityValues) error {

	currentActivity, err := db.client.ActivityValues.Query().Where(activityvalues.HasMonsterWith(entmon.ID(monID))).First(ctx)
	if err != nil {
		logrus.Error("Error adding stuff", err)
		return err
	}

	if currentActivity.Activity != 0 {
		return fmt.Errorf("Already busy with activity %d", currentActivity.Activity)
	}

	activityType := activity.Activity
	activityID := activity.ActivityID

	_, err = db.client.ActivityValues.Update().Where(activityvalues.
		HasMonsterWith(entmon.ID(monID))).
		SetActivity(int(activityType)).
		SetActivityID(activityID).
		Save(ctx)
	if err != nil {
		return err
	}

	return nil

}

func (db *SqliteDB) DeleteMon(ctx context.Context, id int) error {
	logrus.Fatalf("not implemented")
	return nil
}
