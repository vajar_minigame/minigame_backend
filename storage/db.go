package storage

import (
	"context"
	"fmt"

	"gitlab.com/vajar_minigame/minigame_backend/pkg/data/metainfo"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/data/monstermodel"
)

type MonsterDB interface {
	AddMon(ctx context.Context, mon monstermodel.MonsterDto) (int, error)
	GetMonByID(ctx context.Context, id int) (monstermodel.MonsterDto, error)
	GetMonsWithQuery(ctx context.Context, query monstermodel.MonsterQuery) ([]monstermodel.MonsterDto, metainfo.Pagination, error)
	DeleteMon(ctx context.Context, id int) error
	UpdateMon(ctx context.Context, mon monstermodel.MonsterDto) error
	SetActivity(ctx context.Context, monID int, activity monstermodel.ActivityValues) error
}

func BootstrapDBs(monDB MonsterDB) {

	botstrapMonDB(monDB, 1)

}

func botstrapMonDB(m MonsterDB, userID int) {
	//monster

	for i := 0; i < 5; i++ {
		mon := monstermodel.MonsterDto{Name: "blub", UserID: userID, BattleValues: monstermodel.BattleValues{Attack: 10, Defense: 10, MaxHp: 100, RemainingHp: 50}, BodyValues: monstermodel.BodyValues{MaxSaturation: 100, RemainingSaturation: 100, Mass: 50}}

		createdMon, _ := m.AddMon(context.Background(), mon)
		fmt.Printf("Added mon with id: %v\n", createdMon)
	}

	for i := 0; i < 5; i++ {
		mon := monstermodel.MonsterDto{Name: "enemyblub", UserID: -1, BattleValues: monstermodel.BattleValues{Attack: 10, Defense: 10, MaxHp: 100, RemainingHp: 50}, BodyValues: monstermodel.BodyValues{MaxSaturation: 100, RemainingSaturation: 100, Mass: 50}}

		monID, _ := m.AddMon(context.Background(), mon)
		fmt.Printf("Added enemy mon with id: %v\n", monID)
	}
}
