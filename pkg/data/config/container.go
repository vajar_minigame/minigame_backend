package config

import (
	"io/ioutil"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type ContainerConf struct {
	ItemPaths []string `yaml:"itemPaths"`
}

func NewContainerConf(path string) (*ContainerConf, error) {
	yamlData, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Errorf("Error opening file: %v", path)
		return nil, err
	}

	conf := ContainerConf{}

	err = yaml.Unmarshal(yamlData, &conf)
	if err != nil {
		logrus.Errorf("error parsing file %v", err)
	}
	return &conf, err
}
