package monstermodel

import "gitlab.com/vajar_minigame/minigame_backend/ent"

type ActivityType int

const (
	QuestConst ActivityType = iota
	BattleConst
)

type ActivityValues struct {
	Activity   ActivityType `json:"activity" db:"activity"`
	ActivityID int          `json:"activityId" db:"acactivityId"`
}

func ActivityValuesFromEnt(activityEnt *ent.ActivityValues) ActivityValues {
	if activityEnt == nil {
		return ActivityValues{}
	}
	return ActivityValues{Activity: ActivityType(activityEnt.Activity), ActivityID: activityEnt.ActivityID}
}
