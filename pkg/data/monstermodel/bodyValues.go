package monstermodel

import "gitlab.com/vajar_minigame/minigame_backend/ent"

type BodyValues struct {
	RemainingSaturation float64 `json:"remainingSaturation" db:"remainingSaturation"`
	MaxSaturation       float64 `json:"maxSaturation" db:"maxSaturation"`
	Mass                int     `json:"mass" db:"mass"`
}

func BodyValuesFromEnt(bodyEnt *ent.BodyValues) BodyValues {
	return BodyValues{RemainingSaturation: bodyEnt.RemainingSaturation, MaxSaturation: bodyEnt.MaxSaturation, Mass: bodyEnt.Mass}
}
