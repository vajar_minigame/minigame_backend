package monstermodel

import "gitlab.com/vajar_minigame/minigame_backend/ent"

type StatusValues struct {
	IsAlive bool `json:"isAlive" db:"is_alive"`
}

func StatusValuesFromEnt(statusEnt *ent.StatusValues) StatusValues {
	return StatusValues{IsAlive: statusEnt.IsAlive}
}
