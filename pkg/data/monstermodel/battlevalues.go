package monstermodel

import (
	"gitlab.com/vajar_minigame/minigame_backend/ent"
)

type BattleValues struct {
	Attack      int `json:"attack" db:"attack"`
	Defense     int `json:"defense" db:"defense"`
	MaxHp       int `json:"maxHp" db:"maxHp"`
	RemainingHp int `json:"remainingHp" db:"remainingHp"`
}

func BattleValuesFromEnt(battleEnt *ent.BattleValues) BattleValues {
	return BattleValues{Attack: battleEnt.Attack, Defense: battleEnt.Defense, MaxHp: battleEnt.MaxHp, RemainingHp: battleEnt.RemainingHp}
}
