package monstermodel

type MonsterQuery struct {
	UserID int `json:"userId" db:"userId"`

	PageSize  int
	PageIndex int
}
