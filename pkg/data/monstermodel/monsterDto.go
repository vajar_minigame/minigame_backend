package monstermodel

import (
	"time"

	"gitlab.com/vajar_minigame/minigame_backend/ent"
)

type MonsterDto struct {
	ID             int            `json:"id" db:"id"`
	Name           string         `json:"name" db:"name"`
	UserID         int            `json:"userId" db:"userId"`
	LastUpdate     time.Time      `json:"lastUpdate" db:"lastUpdate"`
	IsTest         bool           `json:"isTest" db:"isTest"`
	BattleValues   BattleValues   `json:"battleValues"`
	BodyValues     BodyValues     `json:"bodyValues"`
	StatusValues   StatusValues   `json:"statusValues"`
	ActivityValues ActivityValues `json:"activityValues"`
}

func MonToDto(m MonsterComp, battleValues BattleValues, bodyValues BodyValues, status StatusValues, activity ActivityValues) (MonsterDto, error) {
	return MonsterDto{Name: m.Name, ID: m.ID, UserID: m.UserID, LastUpdate: m.LastUpdate, IsTest: m.IsTest,
		BattleValues: battleValues, BodyValues: bodyValues, ActivityValues: activity, StatusValues: status}, nil
}

func MonsterDtoFromEnt(entity *ent.Monster) MonsterDto {
	return MonsterDto{ID: int(entity.ID), UserID: entity.UserID, Name: entity.Name, LastUpdate: entity.LastUpdated, IsTest: entity.IsTest,
		BattleValues:   BattleValuesFromEnt(entity.Edges.BattleValues),
		BodyValues:     BodyValuesFromEnt(entity.Edges.BodyValues),
		StatusValues:   StatusValuesFromEnt(entity.Edges.StatusValues),
		ActivityValues: ActivityValuesFromEnt(entity.Edges.ActivityValues)}
}
