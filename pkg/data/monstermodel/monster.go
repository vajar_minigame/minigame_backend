package monstermodel

import (
	"time"
)

type MonsterComp struct {
	ID         int       `json:"id" db:"id"`
	Name       string    `json:"name" db:"name"`
	UserID     int       `json:"playerId" db:"userId"`
	LastUpdate time.Time `json:"lastUpdate" db:"lastUpdate"`
	IsTest     bool
}

type Type int

const (
	HP Type = iota + 1
	Saturation
)

var ToType = map[string]Type{
	"hp":         HP,
	"saturation": Saturation,
}
