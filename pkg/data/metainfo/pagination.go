package metainfo

type Pagination struct {
	PageSize   int
	PageIndex  int
	PageCount  int
	PageTCount int
}
