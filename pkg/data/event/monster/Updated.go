package monster

import "gitlab.com/vajar_minigame/minigame_backend/pkg/data/monstermodel"

const UPDATED = "Updated"

type Updated struct {
	Type    string
	Monster monstermodel.MonsterDto
}

func NewMonsterUpdated(mon monstermodel.MonsterDto) Updated {
	return Updated{Type: UPDATED, Monster: mon}
}
