package monster

import "gitlab.com/vajar_minigame/minigame_backend/pkg/data/monstermodel"

const ADDED = "Added"

type Added struct {
	Type    string
	Monster monstermodel.MonsterDto
}

func NewMonsterAdded(mon monstermodel.MonsterDto) Added {
	return Added{Type: ADDED, Monster: mon}
}
