package clientsession

import (
	"context"
	"encoding/json"

	dapr "github.com/dapr/go-sdk/client"
	"github.com/sirupsen/logrus"
)

const pubsubName = "pubsub"

type PubSubClient struct {
	client dapr.Client
	ctx    context.Context
}

func NewPublishClient(client dapr.Client) EventPublisher {
	return &PubSubClient{
		client: client,
		ctx:    context.Background(),
	}
}

func (c *PubSubClient) SendMonsterEvent(event interface{}) error {
	bytes, err := json.Marshal(event)
	if err != nil {
		return err
	}
	logrus.Info("publish monster event")

	err = c.client.PublishEvent(c.ctx, pubsubName, string(MonsterEvent), bytes)
	if err != nil {
		logrus.Debugf("Error sending event %w", err)
		return err
	}

	return nil

}
