package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"gitlab.com/vajar_minigame/minigame_backend/pkg/clientsession"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/data/event/monster"
	"gitlab.com/vajar_minigame/minigame_backend/pkg/data/monstermodel"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/config"
	"gitlab.com/vajar_minigame/minigame_backend/storage"
)

type MonController struct {
	db     storage.MonsterDB
	config *config.Config
	client clientsession.EventPublisher
}

//NewMonService creates a new monservice
func NewMonController(config *config.Config, monDB storage.MonsterDB, client clientsession.EventPublisher) *MonController {

	return &MonController{db: monDB, client: client}

}

func (s *MonController) GetMonstersByQuery(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	queryParams := r.URL.Query()

	queries := monstermodel.MonsterQuery{}

	userIDStr, ok := queryParams["userId"]
	pageSizeStr, ok2 := queryParams["Page-Size"]
	pageIndexStr, ok3 := queryParams["Page-Index"]

	if ok {
		userID, err := strconv.Atoi(userIDStr[0])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logrus.Warn(err)
			fmt.Fprintf(w, "Ooops")
			return
		}
		queries.UserID = userID
	}
	if ok2 {
		pageSize, err := strconv.Atoi(pageSizeStr[0])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logrus.Warn(err)
			fmt.Fprintf(w, "Ooops")
			return
		} else {
			queries.PageSize = pageSize
		}

	} else {
		queries.PageSize = 30
	}
	if ok3 {
		pageIndex, err := strconv.Atoi(pageIndexStr[0])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logrus.Warn(err)
			fmt.Fprintf(w, "Ooops")
			return
		}
		queries.PageIndex = pageIndex
	} else {
		queries.PageIndex = 0
	}

	mons, pagination, err := s.db.GetMonsWithQuery(context.Background(), queries)
	if err != nil {
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			logrus.Error(err)
			fmt.Fprintf(w, "Error")
			return
		}
	}

	// convert to json
	monsJSON, err := json.Marshal(mons)

	if err != nil {
		w.WriteHeader(500)
		logrus.Error(err)
		fmt.Fprintf(w, "Error")
		return
	}
	w.Header().Add("page-Index", fmt.Sprint(pagination.PageIndex))
	w.Header().Add("page-Size", fmt.Sprint(pagination.PageSize))
	w.Header().Add("page-Count", fmt.Sprint(pagination.PageCount))
	w.Header().Add("page-T-Count", fmt.Sprint(pagination.PageTCount))
	w.Write(monsJSON)

}

func (s *MonController) AddMon(w http.ResponseWriter, r *http.Request) {

	ctx := context.Background()
	monDto := monstermodel.MonsterDto{}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error("Could not read body %w", err)
		return
	}
	err = json.Unmarshal(body, &monDto)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error("Could not unmarshal body %w", err)
		return
	}
	addedMonId, err := s.db.AddMon(ctx, monDto)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		return
	}
	addedMon, err := s.db.GetMonByID(ctx, addedMonId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		return
	}
	event := monster.NewMonsterAdded(addedMon)
	s.client.SendMonsterEvent(event)

	logrus.Infof("Mon %v was added", addedMon.ID)

	addedMonByte, err := json.Marshal(addedMon)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		logrus.Infof("Error doin stuff %w", err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(addedMonByte)

}

func (s *MonController) GetMonByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	idStr, ok := vars["id"]

	if ok == false {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Info("id variable not there")
		fmt.Fprintf(w, "Error")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error(err)
		fmt.Fprintf(w, "Ooops")
		return
	}

	mon, err := s.db.GetMonByID(context.Background(), id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		logrus.Info(err)
		fmt.Fprintf(w, "Mon not found")
		return
	}

	serializedMon, err := json.Marshal(mon)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error(err)
		fmt.Fprintf(w, "Serialization failed")
		return
	}
	w.Write(serializedMon)

}

func (s *MonController) UpdateMon(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	idStr, ok := vars["id"]

	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error("id variable not there")
		fmt.Fprintf(w, "Error")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error(err)
		fmt.Fprintf(w, "Ooops")
		return
	}

	ctx := context.Background()
	monDto := monstermodel.MonsterDto{}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error("Could not read body %w", err)
		return
	}
	err = json.Unmarshal(body, &monDto)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error("Could not unmarshal body %w", err)
		return
	}
	monDto.ID = id
	err = s.db.UpdateMon(ctx, monDto)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		return
	}
	serializedMon, err := json.Marshal(monDto)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error(err)
		fmt.Fprintf(w, "Mon not found")
		return
	}
	event := monster.NewMonsterUpdated(monDto)
	s.client.SendMonsterEvent(event)
	w.Write(serializedMon)

	logrus.Infof("Mon %v was updated", monDto.ID)

}
