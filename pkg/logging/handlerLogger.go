package logging

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

func LoggingMiddleware(h http.HandlerFunc) http.HandlerFunc {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		logrus.WithFields(logrus.Fields{
			"URI":    r.URL.String(),
			"Method": r.Method,
		}).Info("Handle route")

		h.ServeHTTP(w, r)

	})
}
