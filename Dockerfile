FROM golang:latest as builder


WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-extldflags '-static'" -o /bin/app



FROM alpine
COPY --from=builder bin/app /go/bin/app
COPY resources /resources
WORKDIR /
ENTRYPOINT ["/go/bin/app"]